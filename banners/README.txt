You can use the spotlight-template.* files when creating a new spotlight banner for the homepage of drupal.org. (Note you will need to remove the trailing .txt in order to open the zip files.)

If you do not use one of the templates, these are the dimensions for spotlight images:
width: 200
height: 221

For consistency, please use Bitstream Vera Sans for spotlights regarding Drupal, drupal.org etc.
Use Bitstream Vera Serif for Drupal Association spotlights.
Download: http://ftp.gnome.org/pub/GNOME/sources/ttf-bitstream-vera/1.10/
